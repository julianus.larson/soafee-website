import { IconType } from '@/components/icon';
import { IconTextLinkItemProps } from '@/components/icon_text_link_item';

// TODO: Missing URLs

export const TermsOfUseItems: IconTextLinkItemProps[] = [
  {
    iconType: IconType.Copy,
    text: 'Content',
    url: '#terms1'
  },
  {
    iconType: IconType.Message,
    text: 'Your Content and Feedback',
    url: '#terms2'
  },
  {
    iconType: IconType.UserCard,
    text: 'Trademarks',
    url: '#terms3'
  },
  {
    iconType: IconType.EditText,
    text: 'Copyright',
    url: '#terms4'
  },
  {
    iconType: IconType.CheckBox,
    text: 'Our Liability and Warranties',
    url: '#terms5'
  },
  {
    iconType: IconType.Shield,
    text: 'Your Indemnities',
    url: '#terms6'
  },
  {
    iconType: IconType.Info,
    text: 'Information about you and your visits to the SOAFEE Website',
    url: '#terms7'
  },
  {
    iconType: IconType.Link2,
    text: 'Links from SOAFEE Website',
    url: '#terms8'
  },
  {
    iconType: IconType.Refresh,
    text: 'Changes to the SOAFEE Website',
    url: '#terms9'
  },
  {
    iconType: IconType.Cog,
    text: 'General',
    url: '#terms10'
  },
  {
    iconType: IconType.File,
    text: 'Changes to the Terms and Conditions of Use',
    url: '#terms11'
  }
];

export default TermsOfUseItems;
