import { ICardWithPagination } from '@/components/cards-with-pagination';
import amd from 'public/logos/amd.png';
import ampere from 'public/logos/ampere.png';
import apexai from 'public/logos/apexai.png';
import aws from 'public/logos/aws.png';
import blackberryqnx from 'public/logos/blackberry-qnx.jpg';
import canonical from 'public/logos/canonical.png';
import continental from 'public/logos/continental.png';
import coreavi from 'public/logos/coreavi.png';
import driveblocks from 'public/logos/driveblocks.png';
import intrepidcs from 'public/logos/intrepidcs.png';
import kpit from 'public/logos/kpit.jpg';
import leddartech from 'public/logos/leddartech.png';
import marvell from 'public/logos/marvell.png';
import mih from 'public/logos/mih.png';
import nxp from 'public/logos/nxp.png';
import panasonicAutomotive from 'public/logos/panasonic_automotive.jpg';
import rti from 'public/logos/rti.png';
import sensory from 'public/logos/sensory.png';
import sonatus from 'public/logos/sonatus.png';
import synopsys from 'public/logos/synopsys.jpg';
import thundersoft from 'public/logos/thundersoft.png';
import tttechAuto from 'public/logos/tttech_auto.png';
import wipro from 'public/logos/wipro.png';

const data: ICardWithPagination[] = [
  {
    body: 'Venetian Convention and Expo Center (Titian 2302-2305)',
    learnMoreLink: '/event/members/amd',
    image: amd
  },
  {
    body: 'COVESA Networking & Showcase - Bellagio Resort',
    learnMoreLink: '/event/members/ampere',
    image: ampere
  },
  {
    body: 'LVCC West Hall #3523',
    learnMoreLink: '/event/members/apex_ai',
    image: apexai
  },
  {
    body: 'LVCC West Hall #4001',
    learnMoreLink: '/event/members/aws',
    image: aws
  },
  {
    body: 'LVCC West Hall #4025',
    learnMoreLink: '/event/members/blackberry_qnx',
    image: blackberryqnx
  },
  {
    body: 'LVCC North Hall #10746',
    learnMoreLink: '/event/members/canonical',
    image: canonical
  },
  {
    body: 'Continental Ballroom @ Renaissance Hotel',
    learnMoreLink: '/event/members/continental',
    image: continental
  },
  {
    body: 'LVCC #6617',
    learnMoreLink: '/event/members/coreavi',
    image: coreavi
  },
  {
    body: 'LVCC North Hall #10175',
    learnMoreLink: '/event/members/driveblocks',
    image: driveblocks
  },
  {
    body: 'LVCC North Hall #3761',
    learnMoreLink: '/event/members/intrepid_control_systems',
    image: intrepidcs
  },
  {
    body: 'LVCC West Hall W318',
    learnMoreLink: '/event/members/kpit',
    image: kpit
  },
  {
    body: 'LVCC West Hall # 5475',
    learnMoreLink: '/event/members/leddartech',
    image: leddartech
  },
  {
    body: 'Venetian Hotel, San Polo 3506 (Level 3)',
    learnMoreLink: '/event/members/marvell',
    image: marvell
  },
  {
    body: 'LVCC West Hall #5274',
    learnMoreLink: '/event/members/mih',
    image: mih
  },
  {
    body: 'LVCC Central Plaza #CP-18',
    learnMoreLink: '/event/members/nxp',
    image: nxp
  },
  {
    body: 'LVCC West Hall #4141',
    learnMoreLink: '/event/members/panasonic_automotive',
    image: panasonicAutomotive
  },
  {
    body: 'LVCC West Hall #4975',
    learnMoreLink: '/event/members/rti',
    image: rti
  },
  {
    body: 'Click to schedule a meeting',
    learnMoreLink: '/event/members/sensory',
    image: sensory
  },
  {
    body: 'LVCC West Hall #4467',
    learnMoreLink: '/event/members/sonatus',
    image: sonatus
  },
  {
    body: 'Click to schedule a meeting',
    learnMoreLink: '/event/members/synopsys',
    image: synopsys
  },
  {
    body: 'LVCC North Hall #10963',
    learnMoreLink: '/event/members/thundersoft',
    image: thundersoft
  },
  {
    body: 'LVCC West Hall #6865',
    learnMoreLink: '/event/members/tttech_auto',
    image: tttechAuto
  },
  {
    body: 'LVCC #6865',
    learnMoreLink: '/event/members/wipro',
    image: wipro
  }
];

export default data;
