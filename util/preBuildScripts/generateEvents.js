import { globSync } from 'glob';
import matter from 'gray-matter';
import fs from 'node:fs';
import * as path from 'path';
import { isWithinNMonthsOfToday } from './date-utils.js';

let imports = `import { Event } from 'data/events';
`;

let exports = `
export default EventsData;
`;

const rootDir = 'pages/event';
const availableFolders = ['2023', '2024']; // Add more folders here if you want to add more years

let generateEvents = async (outputDir) => {
  // Ensure the output directory exists
  if (!fs.existsSync(outputDir)) {
    fs.mkdirSync(outputDir);
  }

  // Find all .mdx pages in the events directory
  let fm = availableFolders.flatMap((folder) => {
    return (
      globSync(`${rootDir}/${folder}/*.mdx`)
        // Extract frontmatter
        .map((file) => {
          let event = matter(fs.readFileSync(file)).data;
          // Extract id from filename, this is guaranteed to be unique as they are all in the same directory
          event.id = path.basename(file, '.mdx');
          event.folder = folder;
          return event;
        })
    );
  });
  // Generate events.ts

  // Generate image imports
  let imageImports = fm.map((data) => {
    const importHeroImage = data.heroImage ? `import ${data.id}Hero from 'public/${data.heroImage}';` : '';
    return `
    import ${data.id} from 'public/${data.image}';
    ${importHeroImage}
    `;
  });
  // Generate data
  let eventsArray = fm
    .sort((a, b) => new Date(b.date) - new Date(a.date)) // Sort by date
    .map((d) => {
      const heroImage = d.heroImage ? `${d.id}Hero` : undefined;
      return `{
    date: '${d.date}',
    title: '${d.title}',
    mobileTitle: '${d.mobileTitle || d.title}',
    body: '${d.body}',
    isHero: ${isWithinNMonthsOfToday(d.date, 3)},
    heroImage: ${heroImage},
    heroBody: '${d.heroBody || d.description}',
    learnMoreLink: 'event/${d.folder}/${d.id}',
    image: ${d.id}
  }`;
    });

  let out = fs.createWriteStream(path.resolve(outputDir, 'events.ts'));

  // Add boilerplate imports
  out.write(imports);
  // Add image imports (so next.js can statically analyze)
  imageImports.map((line) => out.write(line + '\n'));
  // Add the events array
  out.write('\nconst EventsData: Event[] = [\n');
  eventsArray.map((line, i) => out.write(`  ${line}${i === eventsArray.length - 1 ? '' : ','}\n`));
  out.write('];\n');
  // Add the exports definition
  out.write(exports);
};

export default generateEvents;
