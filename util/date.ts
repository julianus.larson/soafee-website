export const formatDate = (dateString: string, userLocale: string) => {
  const options = { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' } as Intl.DateTimeFormatOptions;
  return new Date(dateString).toLocaleString(userLocale, options);
};
