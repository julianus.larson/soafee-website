import { Children, FC } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Pagination, Autoplay } from 'swiper';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import HeroItem from './hero-item';

SwiperCore.use([Pagination, Autoplay]);

interface HeroCarouselProps {
  children: React.ReactNode[];
}

const HeroCarousel: FC<HeroCarouselProps> = ({ children }) => {
  if (!children) return null;

  return (
    <Swiper
      modules={[Pagination]}
      pagination={{ clickable: false }}
      autoplay={{ delay: 5000 }}
      loop={true}
      speed={700}
      slidesPerView={1}
    >
      {Children.map(children, (child) => (
        <SwiperSlide>{child}</SwiperSlide>
      ))}

      <svg fill="#DEDC00" className="absolute right-0 -bottom-1 z-10" height={50} width={100}>
        <polygon points="0, 50 100, 50, 100,0" />
      </svg>
    </Swiper>
  );
};

export { HeroItem };
export default HeroCarousel;
