interface DecoratorProps {
  decoration?: DecorationType;
  decorationClass?: string;
}

export type DecorationType = 'left' | 'right' | 'none';

export default function Decorator({ decoration = 'none', decorationClass = '' }: DecoratorProps) {
  const SVG_CLASS = {
    right: 'visible absolute right-0 -bottom-[0.5px]',
    left: 'visible absolute left-0 -bottom-[0.5px] -scale-x-[1]',
    none: 'hidden'
  };

  return (
    <svg className={`${SVG_CLASS[decoration]} ${decorationClass}`} height={72} width={120}>
      <polygon points="0, 72 120, 72, 120,0" />
    </svg>
  );
}
