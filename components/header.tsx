import Link from 'next/link';
import { useRouter } from 'next/router';
import SOAFEELogo from '@/components/soafeelogo';
import { Icon, IconType } from './icon';
import { useState } from 'react';

interface MenuItem {
  name: string;
  children?: MenuItem[];
  url?: string;
  isExternal?: boolean;
  active?: string[] | string;
}

const aboutMenu: MenuItem[] = [
  { name: 'Accessibility', url: '/about/accessibility' },
  { name: 'Charter', url: '/about/charter' },
  { name: 'Members', url: '/about/members' },
  { name: 'Privacy', url: '/about/privacy' },
  { name: 'Terms of Use', url: '/about/terms_of_use' }
];

const InformationMenu: MenuItem[] = [
  { name: 'Blog', url: '/blog' },
  { name: 'News', url: '/news' },
  {
    name: 'Events',
    url: '/event',
    children: [
      {
        name: 'Presentation materials',
        url: '/event/presentation_materials'
      }
    ]
  },
  { name: 'Integration lab', url: '/community/integration-lab' }
];
// const communityMenu: MenuItem[] = [
//     { name: "Join", url: "/community/join" },
//     { name: "Calendar", url: "/community/calendar" },
//     { name: "GitLab", url: "https://gitlab.com/soafee" },
//     { name: "LinkedIn", url: "https://www.linkedin.com/company/soafee" },
//     { name: "Slack", url: "https://www.soafee.io/community/join" },
// ];

// const InformationMenu: MenuItem[] = [
//     { name: "Blog", url: "/blog" },
//     { name: "News", url: "/news" },
//     { name: "Events", url: "/event" },
// ]

// const documentsMenu: MenuItem[] = [
//     { name: "Architecture", url: "https://architecture.docs.soafee.io" },
// ];

const headerMenu: MenuItem[] = [
  // Left section
  { name: 'About', active: '/about', children: aboutMenu },
  { name: 'Architecture', url: 'https://architecture.docs.soafee.io/en/latest/' },
  { name: 'Information', children: InformationMenu },
  { name: 'Calendar', active: '/community/calendar', url: '/community/calendar' },
  // Right section => isExternal:true
  {
    name: 'Join Our Community',
    isExternal: true,
    url: '/community/join'
  }
];

const GenerateLeftMenu = (items: MenuItem[]) => {
  let route = useRouter().route;

  let isActive = (item: MenuItem, route: string) => {
    let paths = item.active ?? [];
    // Place item into an array if is just a string
    if (!Array.isArray(paths)) paths = [paths];
    // Return true if this menu entry contains the route
    return paths.findIndex((path) => route.startsWith(path)) != -1;
  };

  return items.map((item, index) => {
    let active = isActive(item, route);
    return (
      <div className={`header-item relative flex justify-center `} key={index}>
        <Link
          href={item.url ? item.url : '#'}
          className={`text-[18px] font-[400] ${!active ? 'hover:font-[450]' : ''} leading-6 ${
            active ? 'font-[700]' : ''
          } ${!item.children ? 'cursor-pointer' : 'cursor-default'}`}
          passHref
        >
          {item.name}
        </Link>
        {item.children && (
          <ul className="hidden p-0 absolute w-44 top-full -left-3 bg-soafee-light-gray">
            {item.children.map(({ name, url, children }, index) => (
              <li
                className="group relative hover:bg-soafee-yellow cursor-pointer list-none py-2 px-3 flex"
                key={`sub-${index}`}
              >
                <Link href={url ? url : '#'} passHref className="typography-desktop-b4 flex-1">
                  {name}
                </Link>
                {children && <Icon className="w-[16px] ml-2" type={IconType.ArrowRight} />}
                {children && (
                  <ul className="p-0 absolute w-44 top-0 left-full bg-soafee-light-gray">
                    {children.map(({ name, url }, index) => (
                      <li
                        className="hidden group-hover:flex hover:bg-soafee-yellow cursor-pointer list-none py-2 px-3"
                        key={`sub-${index}`}
                      >
                        <Link href={url ? url : '#'} passHref className="typography-desktop-b4 flex-1">
                          {name}
                        </Link>
                      </li>
                    ))}
                  </ul>
                )}
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  });
};

const generateRightMenu = (items: MenuItem[]) => {
  return items.map((item, index) => {
    return (
      <a className={`text-[18px] font-[700] leading-6 text-soafee-navy mr-7`} href={item.url} key={index}>
        {item.name}
      </a>
    );
  });
};

const SubMenu = ({ submenu, onClose }: { submenu: MenuItem; onClose?: () => void }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const { name, url, children } = submenu;

  return (
    <li className="bg-soafee-yellow typography-mobile-h2 list-none my-2">
      <div className="flex justify-between">
        <Link href={url ? url : '#'} passHref legacyBehavior>
          <p onClick={onClose}>{name}</p>
        </Link>
        {children && (
          <button className="h-auto" onClick={() => setIsExpanded(!isExpanded)}>
            <Icon className="w-[25px] ml-2" type={isExpanded ? IconType.ArrowLeft : IconType.ArrowRight} />
          </button>
        )}
      </div>
      {isExpanded && (
        <ul>
          {children?.map((item, index) => (
            <Link key={`sub-${index}`} href={item.url ? item.url : '#'} passHref legacyBehavior>
              <li onClick={onClose} className="bg-soafee-yellow typography-mobile-h2 list-none my-2">
                {item.name}
              </li>
            </Link>
          ))}
        </ul>
      )}
    </li>
  );
};

const Header = ({ className }: { className: string }) => {
  const [aboutOpen, setAboutOpen] = useState(false);
  const [newsOpen, setNewsOpen] = useState(false);
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  const closeMobileMenu = () => {
    setAboutOpen(false);
    setNewsOpen(false);
    setMobileMenuOpen(false);
  };

  return (
    <div
      className={`${
        mobileMenuOpen ? 'bg-soafee-light-gray' : 'bg-transparent'
      } ${className} w-full absolute top-0 z-50 py-5 md:py-9 wrapper-lg flex font-roboto font-light`}
    >
      <div className="flex">
        <Link href="/" passHref legacyBehavior>
          <div className="cursor-pointer" aria-label="SOAFEE Homepage">
            <SOAFEELogo allowAnimate={false} alt="SOAFEE Homepage" className="w-[86px] lg:w-[132px]" />
          </div>
        </Link>
      </div>
      <div className="flex flex-1 lg:hidden">
        <div onClick={() => setMobileMenuOpen(!mobileMenuOpen)} className="ml-auto flex items-center">
          <Icon className="w-[16px]" type={!mobileMenuOpen ? IconType.Menu : IconType.Close} />
        </div>
        {mobileMenuOpen && (
          <div className={`header-item absolute z-50 top-[62px] left-0  w-screen mr-7 bg-soafee-light-gray`}>
            {headerMenu
              .filter(({ isExternal }) => !isExternal)
              .map((item, index) => (
                <div
                  className={`py-5 px-5 ${
                    (item.name === 'About' && aboutOpen) || (item.name === 'Information' && newsOpen)
                      ? 'bg-soafee-yellow'
                      : ''
                  }`}
                  key={index}
                >
                  {item.url ? (
                    <Link href={item.url ? item.url : '#'} passHref legacyBehavior>
                      <a className="typography-mobile-h1">{item.name}</a>
                    </Link>
                  ) : (
                    <div onClick={() => (item.name === 'About' ? setAboutOpen(!aboutOpen) : setNewsOpen(!newsOpen))}>
                      <a className="typography-mobile-h1 ">{item.name}</a>
                    </div>
                  )}
                  {item.children &&
                    ((item.name === 'About' && aboutOpen) || (item.name === 'Information' && newsOpen)) && (
                      <ul className="bg-soafee-yellow px-0 mt-4">
                        {item.children.map((submenu, index) => (
                          <SubMenu key={`sub-${index}`} submenu={submenu} onClose={closeMobileMenu} />
                        ))}
                      </ul>
                    )}
                </div>
              ))}
          </div>
        )}
      </div>
      <div className="hidden flex-1 justify-between ml-16 lg:flex">
        <div className="flex items-center gap-x-7">
          {GenerateLeftMenu(headerMenu.filter((item) => !item.isExternal))}
        </div>
        <div className="flex items-center">{generateRightMenu(headerMenu.filter((item) => item.isExternal))}</div>
      </div>
    </div>
  );
};

export default Header;
