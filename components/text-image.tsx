import React, { ReactNode } from 'react';

type Props = {
  image: any;
  alt: string;
  imagePosition: 'left' | 'right';
  imageDecorationPosition: 'left' | 'right' | 'none';
  children: ReactNode;
  fill: string;
  childrenClassName: string;
  imageClassName?: string;
  decorationClass?: string;
};

export default function TextImage({
  image,
  alt,
  imagePosition,
  imageDecorationPosition,
  children,
  childrenClassName = 'justify-center py-14',
  imageClassName = ' ',
  decorationClass = 'fill-white',
  fill
}: Props) {
  const IMAGE_POSITION_CLASS: { [key in Props['imagePosition']]: string } = {
    right: 'flex flex-col-reverse lg:flex-row',
    left: 'flex flex-col-reverse lg:flex-row-reverse'
  };
  const SVG_CLASS: { [key in Props['imageDecorationPosition']]: string } = {
    right: 'visible absolute right-0 -bottom-0',
    left: 'visible absolute left-0 -bottom-0 -scale-x-[1]',
    none: 'hidden'
  };
  return (
    <div className={IMAGE_POSITION_CLASS[imagePosition]}>
      <div className={`flex w-full lg:w-1/2 flex-col px-[4%] ${childrenClassName}`}>
        <div className="w-full h-auto">{children}</div>
      </div>
      <div className="px-0 h-[280px] md:h-auto lg:min-h-[730px] w-full lg:w-1/2">
        <div
          className={`h-full !bg-no-repeat !bg-cover !bg-center ${imageClassName}`}
          style={{ background: `url(${image.src})` }}
          aria-label={alt}
        />
        {imageDecorationPosition && (
          <div className="relative">
            <svg
              fill={fill}
              className={`${SVG_CLASS[imageDecorationPosition]} ${decorationClass}`}
              height={72}
              width={120}
            >
              <polygon points="0, 72 120, 72, 120,0" />
            </svg>
          </div>
        )}
      </div>
    </div>
  );
}
