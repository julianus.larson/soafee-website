import { ReactNode, useState } from 'react';
import Icon, { IconType } from './icon';

export interface ExpansionPanelProps {
  id: string;
  children: ReactNode;
  title: string;
  smallText: string;
  image: any;
}

export default function ExpansionPanel({ id, children, title, smallText }: ExpansionPanelProps) {
  const [visible, setVisible] = useState(false);
  return (
    <section className="px-8 py-4" id={id}>
      <div className="typography-tag1">{smallText}</div>
      <div
        className="typography-mobile-h4 lg:typography-b1 border-b-2 lg:border-b border-black border-solid mt-3 mb-2 flex flex-row cursor-pointer"
        onClick={() => setVisible(!visible)}
      >
        <div className="mb-5 lg:mb-2 flex justify-between items-center w-full">
          <div className="lg:mb-3">{title}</div>
          <div className="flex flex-col ml-auto">
            <Icon className="w-3" type={visible === true ? IconType.ArrowUp : IconType.ArrowDown} />
          </div>
        </div>
      </div>
      {visible === true && <div className="typography-b4 mt-16">{children}</div>}
    </section>
  );
}
