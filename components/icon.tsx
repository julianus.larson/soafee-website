import AlignLeft from 'public/icons/align-left.svg';
import ArrowCorner from 'public/icons/arrow-corner.svg';
import ArrowLeft from 'public/icons/arrow-left.svg';
import ArrowRight from 'public/icons/arrow-right.svg';
import ArrowDown from 'public/icons/arrow-down.svg';
import ArrowUp from 'public/icons/arrow-up.svg';
import CheckBox from 'public/icons/check-box.svg';
import Click from 'public/icons/click.svg';
import Close from 'public/icons/close.svg';
import Cog from 'public/icons/cog.svg';
import Cog2 from 'public/icons/cog2.svg';
import Copy from 'public/icons/copy.svg';
import EditText from 'public/icons/edit-text.svg';
import Eye from 'public/icons/eye.svg';
import File from 'public/icons/file.svg';
import Folder from 'public/icons/folder.svg';
import Folder2 from 'public/icons/folder2.svg';
import Info from 'public/icons/info.svg';
import Laptop from 'public/icons/laptop.svg';
import Link from 'public/icons/link.svg';
import Link2 from 'public/icons/link2.svg';
import Menu from 'public/icons/menu.svg';
import Message from 'public/icons/message.svg';
import Refresh from 'public/icons/refresh.svg';
import Shield from 'public/icons/shield.svg';
import Text from 'public/icons/text.svg';
import User from 'public/icons/user.svg';
import User2 from 'public/icons/user-2.svg';
import UserCard from 'public/icons/user-card.svg';
import UserGroup from 'public/icons/user-group.svg';
import Verified from 'public/icons/verified.svg';
import Cookie from 'public/icons/cookie.svg';

enum IconType {
  AlignLeft,
  ArrowCorner,
  ArrowLeft,
  ArrowRight,
  ArrowDown,
  ArrowUp,
  CheckBox,
  Click,
  Close,
  Cog,
  Cog2,
  Copy,
  Cookie,
  EditText,
  Eye,
  File,
  Folder,
  Folder2,
  Info,
  Laptop,
  Link,
  Link2,
  Menu,
  Message,
  Refresh,
  Shield,
  Text,
  User,
  User2,
  UserCard,
  UserGroup,
  Verified
}

type Props = {
  className: string;
  type: IconType;
};

const Icon = ({ className, type }: Props) => {
  switch (type) {
    case IconType.AlignLeft:
      return <AlignLeft className={className} />;
    case IconType.ArrowLeft:
      return <ArrowLeft className={className} />;
    case IconType.ArrowRight:
      return <ArrowRight className={className} />;
    case IconType.ArrowDown:
      return <ArrowDown className={className} />;
    case IconType.ArrowUp:
      return <ArrowUp className={className} />;
    case IconType.ArrowCorner:
      return <ArrowCorner className={className} />;
    case IconType.Folder:
      return <Folder className={className} />;
    case IconType.Info:
      return <Info className={className} />;
    case IconType.User:
      return <User className={className} />;
    case IconType.User2:
      return <User2 className={className} />;
    case IconType.UserGroup:
      return <UserGroup className={className} />;
    case IconType.Link:
      return <Link className={className} />;
    case IconType.Link2:
      return <Link2 className={className} />;
    case IconType.Menu:
      return <Menu className={className} />;
    case IconType.Laptop:
      return <Laptop className={className} />;
    case IconType.Copy:
      return <Copy className={className} />;
    case IconType.Click:
      return <Click className={className} />;
    case IconType.Message:
      return <Message className={className} />;
    case IconType.UserCard:
      return <UserCard className={className} />;
    case IconType.EditText:
      return <EditText className={className} />;
    case IconType.CheckBox:
      return <CheckBox className={className} />;
    case IconType.Shield:
      return <Shield className={className} />;
    case IconType.Refresh:
      return <Refresh className={className} />;
    case IconType.Cog:
      return <Cog className={className} />;
    case IconType.Cog2:
      return <Cog2 className={className} />;
    case IconType.File:
      return <File className={className} />;
    case IconType.Close:
      return <Close className={className} />;
    case IconType.Text:
      return <Text className={className} />;
    case IconType.Folder2:
      return <Folder2 className={className} />;
    case IconType.Verified:
      return <Verified className={className} />;
    case IconType.Eye:
      return <Eye className={className} />;
    case IconType.Cookie:
      return <Cookie className={className} />;
    default:
      throw Error('Unknown IconType ' + type);
  }
};

export { IconType, Icon };
export default Icon;
