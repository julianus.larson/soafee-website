import { Member, Members, MemberType } from 'data//members';
import MemberLogo from './memberlogo';
import { News } from 'data/news';

type Props = {
  type: MemberType;
};

const LogoGrid = (props: Props) => {
  return Members.filter((member: Member) => member.type == props.type)
    .sort((a: Member, b: Member) => {
      const firstLetterNumberRegex = /^[0-9]/;
      const compareFirstLetterAsNumber =
        +firstLetterNumberRegex.test(a.name as string) - +firstLetterNumberRegex.test(b.name as string);
      return compareFirstLetterAsNumber || a.name.localeCompare(b.name);
    })
    .map((member: Member) => {
      const memberNew = News.find((n) => n.image === member.imageData);
      const url = memberNew ? `/${memberNew.learnMoreLink}` : member.website;
      const target = memberNew ? '_self' : '_blank';

      return (
        <div key={member.id} className=" flex justify-center w-[calc(50%-5rem)] lg:w-[calc(25%-5rem)]">
          <a href={url} target={target} rel="noopener noreferrer" aria-label={member.name}>
            <MemberLogo member={member} useInternal={true} />
          </a>
        </div>
      );
    });
};

export default LogoGrid;
