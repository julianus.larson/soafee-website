import Border from './border';
import { IconType, Icon } from './icon';

export interface IconTextLinkItemProps {
  className?: string;
  iconType: IconType;
  text: string;
  url?: string;
  borderClass?: string;
}

export function IconTextLinkItem({
  className = '',
  iconType,
  text,
  url = '',
  borderClass = 'bg-soafee-light-gray'
}: IconTextLinkItemProps) {
  return (
    <div className={`${className} border-l-2 border-l-black border-l-solid px-5 w-64`}>
      <Icon className="w-10 h-10 mb-3" type={iconType} />
      <div className="typography-mobile-h3 lg:typography-desktop-b1 mb-5 min-h-[100px] min-w-[230px]">{text}</div>
      <a href={url}>
        <Border className={borderClass}>Learn More</Border>
      </a>
    </div>
  );
}
