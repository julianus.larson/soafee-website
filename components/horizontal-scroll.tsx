import React, { CSSProperties, ReactNode, useState } from 'react';
import Icon, { IconType } from './icon';

type Props = {
  children: ReactNode;
  style: CSSProperties;
};

interface ContainerState {
  startX: number;
  scrollLeft: number;
  isDown: boolean;
}

const HorizontalScroll = (props: Props) => {
  const [state, setState] = useState<ContainerState>({
    startX: 0,
    scrollLeft: 0,
    isDown: false
  });

  const { startX, scrollLeft, isDown } = state;

  const mouseIsDown = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const container = e.currentTarget as HTMLDivElement;
    setState((prevState) => ({
      ...prevState,
      isDown: true,
      startY: e.pageY - container.offsetTop,
      startX: e.pageX - container.offsetLeft,
      scrollLeft: container.scrollLeft,
      scrollTop: container.scrollTop
    }));
  };

  const mouseUp = () => {
    setState((prevState) => ({
      ...prevState,
      isDown: false
    }));
  };

  const mouseLeave = () => {
    setState((prevState) => ({
      ...prevState,
      isDown: false
    }));
  };

  const mouseMove = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (isDown) {
      e.preventDefault();
      const container = e.currentTarget as HTMLDivElement;
      const x = e.pageX - container.offsetLeft;
      const walkX = x - startX;
      container.scrollLeft = scrollLeft - walkX;
    }
  };

  return (
    <>
      <div className="flex h-3 mb-5 pl-6 lg:pl-8 gap-y-2">
        <Icon className="w-10" type={IconType.ArrowLeft} />
        <Icon className="w-10" type={IconType.ArrowRight} />
      </div>
      <div
        className="w-full flex flex-nowrap no-scrollbar overflow-x-auto"
        style={{ ...props.style }}
        id="items-container"
        onMouseDown={mouseIsDown}
        onMouseUp={mouseUp}
        onMouseLeave={mouseLeave}
        onMouseMove={mouseMove}
      >
        {props.children}
      </div>
    </>
  );
};

export default HorizontalScroll;
