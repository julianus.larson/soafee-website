import { StaticImageData } from 'next/image';
import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
  background: StaticImageData;
};

const Quote = ({ background, children }: Props) => {
  return (
    <div className="!bg-no-repeat !bg-cover !bg-center " style={{ background: `url(${background.src})` }}>
      <div className="bg-soafee-black/[0.800000011920929]">
        <div className="px-5 py-16 lg:p-20">
          <div className="align-self-center m-sm-0 m-md-2 m-lg-4 m-xl-5">{children}</div>
        </div>
      </div>
    </div>
  );
};
export default Quote;
