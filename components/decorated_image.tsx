import Image from 'next/image';
import Decorator, { DecorationType } from './decorator';

type Props = {
  src: any;
  alt: string;
  decoration?: DecorationType;
  decorationClass?: string;
  showSeparator?: boolean;
  imageClassName?: string;
};

const DecoratedImage = ({
  src,
  alt,
  decoration,
  decorationClass,
  showSeparator = false,
  imageClassName = 'w-full h-auto'
}: Props) => {
  return (
    <>
      <Image src={src} alt={alt} className={imageClassName} />
      <Decorator decoration={decoration} decorationClass={decorationClass} />
      {showSeparator && <div className="hidden lg:block absolute h-full w-[1px] bg-soafee-black right-0 top-0" />}
    </>
  );
};

export default DecoratedImage;
