import { useEffect, useState } from 'react';
import Link from 'next/link';
import Cookies from 'universal-cookie';
import Border from './border';
import Icon, { IconType } from './icon';

const cookies = new Cookies();

export interface CookiePermissions {
  version: number;
  allow: boolean;
}

const CookieConsentName = 'cookieConsent';
export const CookieConsentUpdateEvent = 'cookieConsentUpdatedEvent';

export let getCookiePermissions = (): CookiePermissions | null => {
  let value = cookies.get(CookieConsentName);
  if (value != undefined) {
    if (value.version == 1) {
      return value;
    }
  }
  // Return the default permissions if
  return null;
};

const CookieConsent = () => {
  const [show, setShow] = useState(false);
  // Defer setting 'show' based on cookie to ensure tree is always in sync
  useEffect(() => setShow(getCookiePermissions() == null), [show]);

  let accept = (allow: boolean) => {
    // Create a date one year in the future for the cookie expiration
    var date = new Date();
    date.setTime(date.getTime() + 365 * 24 * 60 * 60 * 1000);
    // Set the cookie value
    let cookiePermissions: CookiePermissions = {
      version: 1,
      allow: allow
    };
    cookies.set(CookieConsentName, JSON.stringify(cookiePermissions), {
      expires: date
    });
    // Hide the consent request popup
    setShow(false);

    // Inform the rest of the system if concent has been given
    let event = new CustomEvent<CookiePermissions>(CookieConsentUpdateEvent, {
      detail: cookiePermissions
    });
    document.dispatchEvent(event);
  };

  return show ? (
    <div className="h-screen z-50 fixed bottom-0 left-0 w-full">
      <div className="h-full w-full bg-soafee-black bg-opacity-50" />
      <div className="absolute bottom-0 w-full">
        <div className="wrapper-lg py-4 w-full bg-soafee-yellow block">
          <div className="flex justify-between items-center flex-wrap gap-y-12">
            <div className="flex flex-col gap-x-5 lg:flex-row gap-y-4 mb-4">
              <div className="min-w-[50px] w-[50px] flex justify-center items-center">
                <Icon className="w-full" type={IconType.Cookie} />
              </div>
              <div className="max-w-[650px]">
                <div className="typography-mobile-b1 lg:typography-desktop-b2">This Website Uses Cookies</div>
                <div className="typography-mobile-b3 lg:typography-desktop-b4">
                  This site uses cookies to store information on your computer. By continuing to use our site, you
                  consent to our cookies. If you are not happy with the use of these cookies, please review our{' '}
                  <Link href="/about/privacy" className="underline">
                    <div className="underline inline-block">Cookie Policy</div>
                  </Link>{' '}
                  to learn how they can be disabled. By disabling cookies, some features of the site will not work.
                </div>
              </div>
            </div>
            <button onClick={() => accept(true)}>
              <Border>Accept and hide this message</Border>
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default CookieConsent;
