import type { InferGetServerSidePropsType, GetServerSideProps } from 'next';
import ILTable from '@/components/iltable';
import type { ILRowType, ILColumnType, ILTestrunType } from '@/components/iltable';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Panel, PanelText, PanelLinks, PanelLinkItem } from '@/components/panel';
import DecoratedText from '@/components/decorated_text';
import ImageSeparator from '@/components/image-separator';
import desktopImageSeparator from '../about/images/charter-desktop.jpeg';
import mobileImageSeparator from '../about/images/charter-mobile.png';
import immersive from 'public/hero/immersive.jpg';

type Repo = {
  text: string;
  rows: Array<ILRowType>;
  columns: Array<ILColumnType>;
  testruns: Array<ILTestrunType>;
};

var testrunforenv = new Array();

export const getServerSideProps: GetServerSideProps<{
  repo: Repo;
}> = async () => {
  const res = await fetch('https://qa-reports.linaro.org/api/projects/1469/builds/?ordering=-created_at&format=json');
  const builds = await res.json();
  var build;
  var buildfound = false;
  if (builds.count > 0) {
    build = builds.results[0];
    buildfound = true;
  }
  /*  for (var b of builds.results){
    if(b.finished){
      build = b;
      buildfound = true;
      break;
    }
  };
  */
  if (buildfound) {
    const res1 = await fetch(build.testruns);
    const testruns = await res1.json();
    for (var testrun of testruns.results) {
      const res2 = await fetch(testrun.environment);
      const env = await res2.json();
      if (testrun.build_url) {
        testrunforenv.push({ env: env.slug + 'build', id: testrun.id });
      } else {
        testrunforenv.push({ env: env.slug, id: testrun.id });
      }
    }
  }
  const c: ILColumnType[] = [
    {
      key: 'testsuite',
      header: ' Item'
    },
    {
      key: 'ava',
      header: 'AVA'
    },
    {
      key: 'ava-xen',
      header: 'AVA (Xen)'
    },
    {
      key: 'qemu',
      header: 'QEMU - Arm64 (TRS)'
    },
    {
      key: 'rk3399-rock-pi-4b',
      header: 'RockPi4B'
    },
    {
      key: 'synquacer',
      header: 'Synquacer'
    }
  ];

  const r: ILRowType[] = [
    {
      suitename: 'Build',
      buildversion: build.version
    },
    {
      suitename: 'Boot',
      buildversion: build.version
    },
    {
      suitename: 'Soafee test suite',
      buildversion: build.version
    }
  ];
  const repo: Repo = {
    text: 'Build: ' + build.version + ' Status: ' + (build.finished ? 'Finished' : 'Not finished'),
    rows: r,
    columns: c,
    testruns: testrunforenv
  };
  return { props: { repo } };
};

interface CIResult {
  testsuite: string;
  ava: string;
  ava_xen: string;
  qemu: string;
}

export default function Page({ repo }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (
    <>
      <Panel className="pt-[80px] lg:pt-[102px] bg-soafee-yellow" color="" heroImage={immersive}>
        <div className="max-w-[900px]">
          <PanelText>
            <h2>SOAFEE integration lab at Linaro</h2>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                The SOAFEE Reference Implementation WG develops and maintains a software reference implementation
                (EWAOL) that represents the SOAFEE architecture and ensure its compliance using the SOAFEE test suite
                which is run on several platforms in the SOAFEE Integration Lab. The lab also allows other SOAFEE
                software implementations to be validated against the specification and ensure their compliance.
              </div>
            </div>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                In addition, the SOAFEE Integration lab validates SOAFEE compliant software implementations against
                SOAFEE blueprints to showcase the value of SOAFEE in future Software Defined Vehicle(SDV) architectures.
              </div>
            </div>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">&nbsp;</div>
            </div>
          </PanelText>
        </div>
      </Panel>
      <Panel className="pt-[0px] lg:pt-[0px] bg-soafee-light-gray" color="" heroImage={immersive}>
        <div className="max-w-[900px]">
          <PanelText>
            <h3>Daily test results</h3>
            <div className="mt-5 lg:mt-4">
              <div className="typography-b1">{repo.text}</div>
            </div>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                The results from the last daily run can be seen below. Click on the badge for detailed results. Builds
                can be downloaded from the links below.
              </div>
            </div>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                <ILTable rows={repo.rows} columns={repo.columns} testruns={repo.testruns} />
              </div>
            </div>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                No results yet, tests are in progress:
                <img
                  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=donotexist&suite=donotexist&passrate&title&hide_zeros=1"
                  alt="No results yet, tests are in progress."
                />
              </div>
            </div>
          </PanelText>
        </div>
      </Panel>
      <Panel className="pt-[0px] lg:pt-[0px] bg-soafee-light-gray" color="" heroImage={immersive}>
        <div className="max-w-[900px]">
          <PanelText>
            <h3>Builds</h3>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                <ul>
                  <li>
                    EWAOL - Arm 64bit
                    <Link href="https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ewaol-baremetal-image-ava.rootfs.wic.bz2?job=build-meta-ewaol-machine-avadp">
                      ewaol-baremetal-image-ava.rootfs.wic.bz2
                    </Link>
                  </li>
                  <li>
                    TRS - Arm 64bit
                    <Link href="https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.gz?job=build-meta-trs">
                      trs-image-trs-qemuarm64.rootfs.wic.gz
                    </Link>
                  </li>
                  <li>
                    TRS - Arm 32bit:
                    <Link href="https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm.rootfs.wic.gz?job=build-meta-trs32">
                      trs-image-trs-qemuarm.rootfs.wic.gz
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <h3>FAQs</h3>
            <div className="mt-5 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                <b>Q1. What value does the SOAFEE integration lab provides?</b>
                <p>
                  SOAFEE members who join the integration lab, benefit from the following:
                  <ul>
                    <li>
                      Support the development and regression detection of EWAOL (if available) on their HW platforms
                    </li>
                    <li>Continued compliance monitoring via the SOAFEE compliance test suite</li>
                    <li>Ensure SOAFEE compliance of non-EWAOL OS builds (eg. AutoSD from RedHat)</li>
                    <li>Provide the needed CI to monitor and support SOAFEE blueprints development</li>
                  </ul>
                </p>
                &nbsp;
              </div>
            </div>
            <div className="mt-0 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                <b>Q2. What do I need to join the integration lab?</b>
                <p>
                  Requirements to join the lab can vary depending on the device, software and blueprints. Please contact
                  us for details. In general, the requirements span the following:
                  <ul>
                    <li>Setup remote lab worker</li>
                    <li>Provide BSP for target HW platform (If not System Ready compliant)</li>
                    <li>Enable device automation in LAVA</li>
                    <li>Integrate CI results in current dashboards</li>
                  </ul>
                </p>
                &nbsp;
              </div>
            </div>
            <div className="mt-0 lg:mt-4">
              <div className="typography-mobile-b2 lg:typography-desktop-b4">
                <b>Q2. How do I join the SOAFEE Integration lab?</b>
                <p>
                  Please
                  <a href="mailto:anmar.oueja@linaro.org"> contact the SOAFEE Reference Implementation WG chair</a> for
                  details.
                </p>
                &nbsp;
              </div>
            </div>
          </PanelText>
        </div>
      </Panel>
    </>
  );
}
